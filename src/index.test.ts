import {sayHello} from "./index";


describe('test', () => {
   test('should work', () => {
       expect(sayHello()).toBe('Hello bar');
   });

   test('should work 2', () => {
       expect(sayHello('Kevin')).toBe('Hello Kevin');
   });
});