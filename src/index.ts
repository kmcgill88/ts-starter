export const sayHello = (foo?: string): string => {
    const name = foo ?? "bar";

    return `Hello ${name}`;
};